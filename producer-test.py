from kafka import KafkaProducer
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--server', default="localhost:9094",
                    type=str, help="ip address of server")
parser.add_argument('-t', '--topic', default='basic_topic',
                    type=str, help='Kafka topic')
parser.add_argument('-m', '--msg', default='test_msg',
                    type=str, help='message to send')
parser.add_argument('--loop', default=False, action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    try:
        producer = KafkaProducer(bootstrap_servers=args.server)
        print("conntected to server")
        producer.send(args.topic, str.encode(args.msg))
        while args.loop:
            producer.send(args.topic, str.encode(args.msg))

    except:
        print("not able to connect with server")
