import json
import os

class ConfigParser:

    CONFIG_FILE_NAME = 'config.json'

    def __init__(self):
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname + '/',self.CONFIG_FILE_NAME)

        with open(filename) as f:
            self.data = json.load(f)

    def getAuthUser(self):
        return self.data["network"]["auth"]["user"]

    def getAuthPassword(self):
        return self.data["network"]["auth"]["password"]