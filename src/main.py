import cv2
import numpy as np
import socket
import requests
from PIL import Image
from six import StringIO
from configparser import ConfigParser
from time import sleep
import pprint
import json
cap = cv2.VideoCapture(0)

configParser = ConfigParser()

while True and cap.isOpened():
	ret, frame = cap.read()
	if ret == False:
		print("Frame is empty")
		continue
	cv2.imwrite('./frame.jpg', frame)

	print(configParser.getAuthUser())

	pil_im = Image.fromarray(frame)
	stream = StringIO()
	pil_im.save(stream, format="JPEG")
	stream.seek(0)
	im_for_post = stream.read()
	files = {'image': open('./frame.jpg', 'rb')}
	authUser = configParser.getAuthUser()
	authPass = configParser.getAuthPassword()

	response = requests.post(
		url="http://www.dukycam.auenlandchen.de/index.php",
		files=files,
		auth=(authUser, authPass)
	)


	print response.content

cap.release()
cv2.destroyAllWindows()
