DukyCam
=========

## Setting up a consumer

Map in ```/etc/hosts``` the hostname ```dukykafka``` to the IP of the kafka server.
Make sure to allow connections on port 9094.

## Installing on Raspberry PI

* OpenCV: follow for example [this tutorial](https://handtoolsforfun.com/how-to-install-opencv-in-a-raspberry-pi-3-b/)
* PiCamera
```
pip3 install "picamera[array]"
```
