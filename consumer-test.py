from kafka import KafkaConsumer
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-s', '--server', default="localhost:9094",
                    type=str, help="IP address and port of server")
parser.add_argument('-t', '--topic', default='basic_topic',
                    type=str, help='Kafka topic')

if __name__ == '__main__':
    args = parser.parse_args()
    try:
        consumer = KafkaConsumer(args.topic, bootstrap_servers=args.server)
        print("connected to server")
        while True:
            for msg in consumer:
                print(msg.value)
    except:
        print("not able to connect with server")
