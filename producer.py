import sys
import time
import cv2
from kafka import KafkaProducer
import numpy as np
import argparse

fourcc = cv2.VideoWriter_fourcc(*'DIVX')
out = cv2.VideoWriter('motiondetec.avi', fourcc, 60.0,
                      (1920, 1080), isColor=False)

foreground = cv2.createBackgroundSubtractorMOG2(detectShadows=False)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--id', default=0, type=int,
                    help='the id of the device')


def publish_camera(camera_id):
    topic = f"kafka_video_{camera_id}"
    producer = KafkaProducer(bootstrap_servers='dukykafka:9094')
    camera = cv2.VideoCapture(0)
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    camera.set(cv2.CAP_PROP_FPS, 6)

    try:
        while(True):
            success, camframe = camera.read()
#            grayframe = cv2.cvtColor(camframe, cv2.COLOR_BGR2GRAY)
#            blurframe = cv2.GaussianBlur(camframe, (5, 5), 0)
#            motionframe = foreground.apply(grayframe)
#            detect = (np.sum(motionframe))//255
#
            ret, buffer = cv2.imencode('.jpg', camframe)
            producer.send(topic, buffer.tobytes())

            time.sleep(0.2)
    except:
        print("\nWe are done.")
        sys.exit(1)

    camera.release()
    out.release()


if __name__ == '__main__':
    args = parser.parse_args()
    publish_camera(args.id)
