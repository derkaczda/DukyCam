import datetime
from flask import Flask, Response, render_template
from kafka import KafkaConsumer


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/video_feed/<camera_id>')
def video_feed(camera_id):
    topic = f"kafka_video_{camera_id}"
    consumer = KafkaConsumer(
        topic,
        bootstrap_servers=['dukykafka:9094']
    )
    return Response(
        get_video_stream(consumer),
        mimetype='multipart/x-mixed-replace; boundary=frame'
    )


def get_video_stream(consumer):
    for msg in consumer:
        yield (b'--frame\r\n'
               b'Content-Type: image/jpg\r\n\r\n' + msg.value + b'\r\n\r\n')


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
